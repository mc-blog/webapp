import React from 'react';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducers from './reducers'
import Layout from './containers/LayoutComponent';
import '../styles/main.scss';

const store = createStore(reducers);

let { search } = location;
const tokenReg = /\?token=/;
if (tokenReg.test(search)) {
  const token = search.replace(tokenReg, '');
  localStorage.setItem('token', token);
  window.history.replaceState({}, '', '/');
}

const App = () => (
  <Provider store={store}>
    <Layout />
  </Provider>
);

render(<App/>, document.getElementById('app'));