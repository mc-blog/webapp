import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { AppBar, Button, IconButton, Toolbar, Typography } from '@material-ui/core';
import { Menu } from '@material-ui/icons';

const HeaderComponent = ({ user }) => {
  return (
    <Fragment>
      <AppBar position='static'>
        <Toolbar>
          <IconButton color="inherit" aria-label="Menu">
            <Menu />
          </IconButton>
          <Typography variant="title" color="inherit" className="header-title">
            mc-blog
          </Typography>
          {user ? (
            <Typography color="inherit">
              {user.username}
            </Typography>
          ) : (
            <Button
              color="inherit"
              component="a"
              href="http://localhost:3000/auth/google"
            >
              Login with google
            </Button>
          )}
        </Toolbar>
      </AppBar>
    </Fragment>
  )
};

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(HeaderComponent);