import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Button, Card, TextField } from '@material-ui/core';
import axios from 'axios';

class CreateComponent extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      content: '',
    };

    this.add = this.add.bind(this);
    this.handleChangeTitle = this.handleChangeTitle.bind(this);
    this.handleChangeContent = this.handleChangeContent.bind(this);
  }

  componentDidMount() {
    const token = localStorage.getItem('token');
    if (!token) {
      this.props.history.push('/');
    }
  }

  handleChangeTitle(e) {
    this.setState({
      ...this.state,
      title: e.target.value,
    })
  }

  handleChangeContent(e) {
    this.setState({
      ...this.state,
      content: e.target.value,
    })
  }

  add() {
    const { content, title } = this.state;
    const token = localStorage.getItem('token');
    axios
      .post('http://localhost:3001/posts', { content, title, token })
      .then(console.log);
    this.props.history.push('/');
  }

  render() {
    const { content, title } = this.state;
    return (
      <Card>
        <div className="content">
          <TextField
            className="text"
            label="Title"
            type="text"
            margin="normal"
            value={title}
            onChange={this.handleChangeTitle}
          />
          <TextField
            className="text"
            label="Content"
            type="text"
            margin="normal"
            rows={3}
            rowsMax={15}
            value={content}
            onChange={this.handleChangeContent}
            multiline
          />
          <Button
            color="secondary"
            variant="raised"
            className="btn"
            onClick={this.add}
          >
            Add post
          </Button>
        </div>
      </Card>
    )
  }
}

export default withRouter(CreateComponent);