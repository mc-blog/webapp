import React from "react";
import { Button, Card, CardActions, CardContent, CardHeader, Grid, Typography } from "@material-ui/core";

const PostComponent = ({ author, date, title, content }) => {
  return (
    <Grid item sm={6} xs={12}>
      <Card>
        <CardHeader
          title={title}
          subheader={`${author} / ${date}`}
        >
        </CardHeader>
        <CardContent>
          <Typography component="p">
            {content}
          </Typography>
        </CardContent>
        <CardActions>
          <Button variant="raised" color="primary">
            more
          </Button>
        </CardActions>
      </Card>
    </Grid>
  )
};

export default PostComponent;