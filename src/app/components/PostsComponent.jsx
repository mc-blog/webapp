import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { CircularProgress, Grid } from '@material-ui/core';
import axios from 'axios';
import Post from '../components/PostComponent';

class LayoutComponent extends Component {
  componentDidMount() {
    axios
      .get('http://localhost:3001/posts')
      .then((posts) => {
        console.log(posts);
      });
  }

  render() {
    const { posts } = this.props;
    return (
      <Grid container spacing={8}>
        {posts.length ? (
          <Fragment>
            <Post
              author="Mateusz Czeplowski"
              date="23.01.2012"
              title="Siema siema"
              content="Witam na blogu elo elo elo Witam na blogu elo elo elo Witam na blogu elo elo elo Witam na blogu elo elo elo "
            />
            <Post
              author="Mateusz Czeplowski"
              date="23.01.2012"
              title="Siema siema"
              content="Witam na blogu elo elo elo Witam na blogu elo elo elo Witam na blogu elo elo elo Witam na blogu elo elo elo "
            />
            <Post
              author="Mateusz Czeplowski"
              date="23.01.2012"
              title="Siema siema"
              content="Witam na blogu elo elo elo Witam na blogu elo elo elo Witam na blogu elo elo elo Witam na blogu elo elo elo "
            />
            <Post
              author="Mateusz Czeplowski"
              date="23.01.2012"
              title="Siema siema"
              content="Witam na blogu elo elo elo Witam na blogu elo elo elo Witam na blogu elo elo elo Witam na blogu elo elo elo "
            />
            <Post
              author="Mateusz Czeplowski"
              date="23.01.2012"
              title="Siema siema"
              content="Witam na blogu elo elo elo Witam na blogu elo elo elo Witam na blogu elo elo elo Witam na blogu elo elo elo "
            />
          </Fragment>
        ) : (
          <CircularProgress size={50} className="loader" />
        )}
      </Grid>
    )
  }
}

const mapStateToProps = state => ({
  posts: state.posts,
});

export default connect(mapStateToProps)(LayoutComponent);