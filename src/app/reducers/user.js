const user = (state = null, action) => {
  switch(action.type) {
    case 'SET_USER_DATA':
      return action.userData;
    case 'RESET_USER':
      return null;
    default:
      return state;
  }
};

export default user;
