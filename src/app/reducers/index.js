import { combineReducers } from 'redux';

import create from './create';
import user from './user';
import posts from './posts';

const reducers = combineReducers({
  create,
  user,
  posts
});

export default reducers;
