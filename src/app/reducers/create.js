const initState = {
  title: '',
  content: '',
};

const create = (state = initState, action) => {
  switch(action.type) {
    case 'SET_TITLE':
      return {
        ...state,
        title: action.title,
      };
    case 'SET_CONTENT':
      return {
        ...state,
        content: action.content,
      };
    default:
      return state;
  }
};

export default create;
