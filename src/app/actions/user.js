export const setUserData = userData => ({
  type: 'SET_USER_DATA',
  userData,
});

export const resetUser = () => ({
  type: 'RESET_USER'
});