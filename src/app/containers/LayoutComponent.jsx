import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect, Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import * as jwt from 'jsonwebtoken';
import Header from '../components/HeaderComponent';
import { setUserData } from '../actions/user'
import Posts from '../components/PostsComponent';
import Create from '../components/CreateComponent';

class LayoutComponent extends Component {
  componentDidMount() {
    const token = localStorage.getItem('token');
    if (token) {
      const userData = jwt.verify(token, 's@cr@t');
      this.props.setUserData(userData);
    }
  }

  render() {
    return (
      <Router>
        <Fragment>
          <Header />
          <Switch>
            <Route
              exact
              path="/"
              component={Posts}
            />
            <Route
              path="/create"
              component={Create}
            />
            <Redirect to="/" />
          </Switch>
        </Fragment>
      </Router>
    )
  }
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    setUserData
  }, dispatch)
);

export default connect(null, mapDispatchToProps)(LayoutComponent);